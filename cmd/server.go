package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/juaninsis/go-chat-server/internal/cache"
	"gitlab.com/juaninsis/go-chat-server/internal/cfg"
	"gitlab.com/juaninsis/go-chat-server/internal/chat"
	"gitlab.com/juaninsis/go-chat-server/internal/queue"
)

const (
	serverStatus = "🗣️ Chat server "
)

var (
	conf                = cfg.GetConfig()
	logoutSessionsTopic = "logout-sessions"
	kafkaURL            = conf.KafkaServerURL + ":" + conf.KafkaServerPort
)

func init() {
	loggerSettings()
}

func main() {
	queueCli := queue.NewKafkaClient([]string{kafkaURL})
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println(serverStatus + "ending...")
		msgs := make([][]byte, 0)
		for _, v := range chat.Clients {
			msgs = append(msgs, []byte(v.Metadata.SessionID))
		}
		queueCli.PushCommentToQueue(logoutSessionsTopic, msgs)
		os.Exit(1)
	}()
	buildChatServer(queueCli)
	log.Info(serverStatus + "listening...")

	log.Fatal(http.ListenAndServe(":"+conf.Port, nil))
}
func buildChatServer(queueCli queue.KafkaClient) {
	redisCli := cache.NewRedis(redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", conf.RedisHost, conf.RedisPort),
		Password: "",
		DB:       0,
	}))
	p := chat.NewPlatform(redisCli, &http.Client{}, queueCli)
	a := chat.NewAction(p)
	chat.Serve(&websocket.Upgrader{}, a)
}
func loggerSettings() {
	log.SetFormatter(&log.JSONFormatter{})
	if conf.LogPath != "" {
		if f, err := os.OpenFile(conf.LogPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666); err == nil {
			log.SetOutput(f)
		}
	}
	log.SetLevel(log.Level(conf.LogLevel))
}
