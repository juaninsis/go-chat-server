build:
	docker build --no-cache -t go-chat-server-app -f ./Dockerfile.test .

run:
	docker run --network chat-net  -p 5554:5554 --env-file .env -i go-chat-server-app

deploy: build run

up:
	docker-compose  up --build --detach

down:
	docker-compose down

logs:
	docker logs --follow go-chat-server_go-chat-server_1

app: up logs

generate-mock:
	mockery --inpackage --all

status := $(if $(shell docker ps -a -q),ok,notok)
docker-clean:
ifeq ($(status), ok)
	@docker stop $(shell docker ps -a -q) && docker rm $(shell docker ps -a -q) && docker volume prune -f
	@echo "the stack of containers was cleaned"
else
	@echo "the stack of containers is empty"
endif