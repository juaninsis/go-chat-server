# go-chat-server



## Description

You are tasked with developing a microservice that handles real-time communication between users via websockets. The microservice should allow users to send messages to each other and subscribe to specific topics or channels to receive updates.

The goal of this challenge is to evaluate your proficiency in the following areas:

- Microservice architecture design
- Websocket communication
- Implementation of concurrency and synchronization
- Error handling
- Unit testing
- Git usage


## Deployment

Every deployment commands are in the Makefile
Example:
```
## deploy server
make up

## watch logs
make logs
```