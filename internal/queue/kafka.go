package queue

import (
	"context"
	"fmt"

	"github.com/Shopify/sarama"
	log "github.com/sirupsen/logrus"
	"gitlab.com/juaninsis/go-chat-server/internal/cfg"
)

var (
	conf         = cfg.GetConfig()
	kafkaURL     = []string{conf.KafkaServerURL + ":" + conf.KafkaServerPort}
	KafkaVersion = conf.KafkaServerVersion
)

type (
	kafkaClient struct {
		brokers []string
	}

	KafkaClient interface {
		ConnectProducer() (sarama.SyncProducer, error)
		PushCommentToQueue(topic string, messages [][]byte) error
		PushCommentToQueueWithConn(producer sarama.SyncProducer, topic string, messages [][]byte) error
		StartConsumerWithGroupID(ctx context.Context, groupID, kafkaVersion string, topics []string, chMsg chan []byte)
		StartConsumer(ctx context.Context, topic string, chMsg chan []byte)
	}
)

func NewKafkaClient(brokers []string) KafkaClient {
	return &kafkaClient{
		brokers: brokers,
	}
}

func (kc *kafkaClient) ConnectProducer() (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5
	conn, err := sarama.NewSyncProducer(kc.brokers, config)
	if err != nil {
		return nil, err
	}
	return conn, nil
}
func (kc *kafkaClient) PushCommentToQueueWithConn(producer sarama.SyncProducer, topic string, messages [][]byte) error {
	for _, message := range messages {
		msg := &sarama.ProducerMessage{
			Topic: topic,
			Value: sarama.StringEncoder(message),
		}
		_, _, err := producer.SendMessage(msg)
		if err != nil {
			return err
		}
	}
	return nil
}

func (kc *kafkaClient) PushCommentToQueue(topic string, messages [][]byte) error {
	producer, err := kc.ConnectProducer()
	if err != nil {
		return err
	}
	defer producer.Close()
	for _, message := range messages {
		msg := &sarama.ProducerMessage{
			Topic: topic,
			Value: sarama.StringEncoder(message),
		}
		_, _, err := producer.SendMessage(msg)
		if err != nil {
			return err
		}
	}
	return nil
}

type Message struct {
	chMsg     chan []byte
	topic     string
	partition int32
}

func (Message) Setup(_ sarama.ConsumerGroupSession) error   { return nil }
func (Message) Cleanup(_ sarama.ConsumerGroupSession) error { return nil }
func (h Message) ConsumeClaim(sess sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	defer func() {
		if r := recover(); r != nil {
			log.Error("[func: consume_claim] [status: finished_with_panic]")
			return
		}
	}()
	fmt.Println(claim.Partition(), claim.Topic())

	log.Debug("[func: consume_claim] [status: started]")
	for msg := range claim.Messages() {
		h.chMsg <- msg.Value
		sess.MarkMessage(msg, "")
	}
	return nil
}

func (kc *kafkaClient) connectConsumer() (sarama.Consumer, error) {
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	conn, err := sarama.NewConsumer(kc.brokers, config)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func consumeClaim(ctx context.Context, consumer sarama.PartitionConsumer, chMsg chan []byte) {
	log.Debug("[func: consume_claim] [status: started]")
	for {
		select {
		case msg := <-consumer.Messages():
			chMsg <- msg.Value
		case err := <-consumer.Errors():
			log.Errorf("[func: consume_claim] [error: %+v]", err)
		case <-ctx.Done():
			fmt.Println("ctx finished")
			return
		}
	}
}

func (kc *kafkaClient) StartConsumer(ctx context.Context, topic string, chMsg chan []byte) {
	worker, err := kc.connectConsumer()
	if err != nil {
		log.Errorf("[func: start_consumer_connect_consumer] [error: %+v]", err)
		return
	}
	consumer, err := worker.ConsumePartition(topic, 0, sarama.OffsetNewest)

	if err != nil {
		log.Errorf("[func: start_consumer_consume_partition] [error: %+v]", err)
		return
	}
	consumeClaim(ctx, consumer, chMsg)
}

func (kc *kafkaClient) StartConsumerWithGroupID(ctx context.Context, groupID, kafkaVersion string, topics []string, chMsg chan []byte) {
	defer func() {
		if r := recover(); r != nil {
			log.Error("[func: start_consumer_with_group_id] [status: panic]")
		}
	}()
	client, err := createSaramaClient()
	if err != nil {
		log.Errorf("[func: start_consumer_with_group_id_new_client] [error: %+v]", err)
		return
	}
	defer client.Close()

	group, err := sarama.NewConsumerGroupFromClient(groupID, client)
	if err != nil {
		log.Errorf("[func: start_consumer_with_group_id_new_consumer] [error: %+v]", err)
		return
	}
	defer group.Close()

	_ = context.Background()
	for {
		handler := &Message{chMsg: chMsg, topic: topics[0]}
		if err = group.Consume(ctx, topics, handler); err != nil {
			log.Errorf("[func: start_consumer_with_group_id_loop] [error: %+v]", err)
			break
		}
	}
}

func createSaramaClient() (sarama.Client, error) {

	config, err := getDefaultConsumerConfig()
	if err != nil {
		log.Errorf("[func: start_consumer_parse_kafka_version] [error: %+v]", err)
		return nil, err
	}
	// Start with a client
	client, err := sarama.NewClient(kafkaURL, config)
	if err != nil {
		log.Errorf("[func: start_consumer_sarama_new_client] [error: %+v]", err)
		return nil, err
	}
	return client, nil
}

func getDefaultConsumerConfig() (*sarama.Config, error) {
	kfversion, err := sarama.ParseKafkaVersion(conf.KafkaServerVersion)
	if err != nil {
		return nil, err
	}

	config := sarama.NewConfig()
	config.Version = kfversion
	config.Consumer.Return.Errors = true
	config.ClientID = "chat-server"
	return config, nil
}
