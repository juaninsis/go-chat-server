package chat

import (
	"context"
	"errors"
	"testing"

	mock "github.com/stretchr/testify/mock"
)

func TestGetProfile(t *testing.T) {
	pm := MockPlatformer{}
	pm.On("GetProfile", mock.Anything, mock.Anything).Return(nil, nil)
	a := NewAction(&pm)
	_, _ = a.GetProfile(context.Background(), "new_topic", "id_token")
}

func TestSetConnection(t *testing.T) {
	tests := []struct {
		name          string
		inputConnMock WebSocketConnecter
		inputMetadata Metadata
		pMock         Platformer
	}{
		{
			name:          "conn error set_connection case",
			inputMetadata: Metadata{},
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "cmdTest error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "no msg error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(``), nil).Once()
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "no msg error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(`{text":"/test"}`), nil).Once()
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "no msg error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/group test1"}`), nil).Once()
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "no msg error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/chat test1"}`), nil).Once()
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "no msg error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/general"}`), nil).Once()
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "no msg error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"something"}`), nil).Once()
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("SendMessage", mock.Anything, mock.Anything).Return(nil)
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
		{
			name:          "no msg error set_connection case",
			inputMetadata: Metadata{},
			inputConnMock: func() WebSocketConnecter {
				wsm := MockWebSocketConnecter{}
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"something"}`), nil).Once()
				wsm.On("ReadMessage").Return(0, []byte(`{"text":"/test"}`), nil)
				return &wsm
			}(),
			pMock: func() Platformer {
				pm := MockPlatformer{}
				_, cancel := context.WithCancel(context.Background())
				pm.On("CatchMessages", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("CatchMessagesWithGroupID", mock.Anything, mock.Anything, mock.Anything).Return(cancel)
				pm.On("InitMsg", mock.Anything)
				pm.On("UserOnMsg", mock.Anything)
				pm.On("SendMessage", mock.Anything, mock.Anything).Return(errors.New("some error"))
				pm.On("UserOffMsg", mock.Anything)
				pm.On("Logout", mock.Anything, mock.Anything).Return(nil)
				return &pm
			}(),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			a := NewAction(test.pMock)
			a.SetConnection(test.inputConnMock, test.inputMetadata)
		})
	}
}
