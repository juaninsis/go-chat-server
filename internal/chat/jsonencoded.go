package chat

import (
	"context"

	log "github.com/sirupsen/logrus"
)

type (
	ClientConn map[string]Client
	Client     struct {
		Username string `json:"username"`
		Conn     WebSocketConnecter
		Metadata Metadata
	}

	Message struct {
		Origin      string    `json:"origin"`
		Text        string    `json:"text"`
		Destination string    `json:"destination"`
		IsBroadcast bool      `json:"is_broadcast"`
		Timestamp   int64     `json:"timestamp"`
		Metadata    *Metadata `json:"metadata"`
		Error       *Error    `json:"error"`
		Topic       string    `json:"topic"`
		Type        string    `json:"type"`
	}
	Metadata struct {
		Name      string `json:"name"`
		Nickname  string `json:"nickname"`
		SessionID string `json:"sid"`
		UserID    string `json:"sub"`
	}
	Error struct {
		Type string `json:"type"`
		Text string `json:"text"`
	}

	MsgInfo struct {
		CtxCancel context.CancelFunc
		ChMsg     chan []byte
	}

	groupsM map[string]MsgInfo
)

func (cc ClientConn) Delete(key string) {
	delete(cc, key)
}

func (gm groupsM) addConsumer(p Platformer, topic string, metadata Metadata) {
	if _, ok := gm[topic]; !ok {
		for consumerName := range gm {
			if consumerName != broadcast {
				if v, ok := gm[consumerName]; ok {
					v.CtxCancel()
					log.Debugf("[func: add_consumer] [consumer_name: %s]", consumerName)
				}
			}
		}
		chMsg := make(chan []byte)
		log.Debugf("[func: add_consumer] [topic_added: %s]", topic)
		if metadata.UserID == topic {
			gm[topic] = MsgInfo{CtxCancel: p.CatchMessagesWithGroupID(topic, metadata, chMsg), ChMsg: chMsg}
			return
		}
		gm[topic] = MsgInfo{CtxCancel: p.CatchMessages(topic, metadata, chMsg), ChMsg: chMsg}
	}
}
