// Code generated by mockery v2.14.1. DO NOT EDIT.

package chat

import mock "github.com/stretchr/testify/mock"

// MockWebSocketConnecter is an autogenerated mock type for the WebSocketConnecter type
type MockWebSocketConnecter struct {
	mock.Mock
}

// Close provides a mock function with given fields:
func (_m *MockWebSocketConnecter) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ReadMessage provides a mock function with given fields:
func (_m *MockWebSocketConnecter) ReadMessage() (int, []byte, error) {
	ret := _m.Called()

	var r0 int
	if rf, ok := ret.Get(0).(func() int); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int)
	}

	var r1 []byte
	if rf, ok := ret.Get(1).(func() []byte); ok {
		r1 = rf()
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).([]byte)
		}
	}

	var r2 error
	if rf, ok := ret.Get(2).(func() error); ok {
		r2 = rf()
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// WriteMessage provides a mock function with given fields: messageType, data
func (_m *MockWebSocketConnecter) WriteMessage(messageType int, data []byte) error {
	ret := _m.Called(messageType, data)

	var r0 error
	if rf, ok := ret.Get(0).(func(int, []byte) error); ok {
		r0 = rf(messageType, data)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewMockWebSocketConnecter interface {
	mock.TestingT
	Cleanup(func())
}

// NewMockWebSocketConnecter creates a new instance of MockWebSocketConnecter. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMockWebSocketConnecter(t mockConstructorTestingTNewMockWebSocketConnecter) *MockWebSocketConnecter {
	mock := &MockWebSocketConnecter{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
