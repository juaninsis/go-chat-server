package chat

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/juaninsis/go-chat-server/internal/cache"
)

type (
	platform struct {
		client *http.Client
		cache  cache.Redis
		queue  Queue
	}
	Platformer interface {
		GetProfile(token, idToken string) (map[string]interface{}, error)
		SetProfileCache(ctx context.Context, metadata Metadata)
		GetProfileCache(ctx context.Context, key string) (*Metadata, error)
		Logout(ctx context.Context, sid string) error
		CatchMessagesWithGroupID(topic string, metadata Metadata, chMsg chan []byte) context.CancelFunc
		CatchMessages(topic string, metadata Metadata, chMsg chan []byte) context.CancelFunc
		SendMessage(topic string, messages [][]byte) error
		InitMsg(metadata Metadata)
		UserHealthMsg(metadata Metadata)
		UserOnMsg(metadata Metadata)
		UserOffMsg(metadata Metadata)
		UserInfoMsg(metadata, metadataInfo Metadata)
		HelpMsg(text string, metadata Metadata)
	}
	Queue interface {
		PushCommentToQueue(topic string, messages [][]byte) error
		StartConsumerWithGroupID(ctx context.Context, groupID, kafkaVersion string, topics []string, chMsg chan []byte)
		StartConsumer(ctx context.Context, topics string, chMsg chan []byte)
	}
)

func NewPlatform(cache cache.Redis, client *http.Client, queue Queue) Platformer {
	return &platform{client: client, cache: cache, queue: queue}
}

func (p *platform) SetProfileCache(ctx context.Context, metadata Metadata) {
	p.cache.Set(ctx, metadata.UserID, metadata, 0)
}

func (p *platform) GetProfileCache(ctx context.Context, key string) (*Metadata, error) {
	b, err := p.cache.Get(ctx, key)
	if err != nil {
		return nil, err
	}
	metadata := Metadata{}
	if err := json.Unmarshal(b, &metadata); err != nil {
		return nil, err
	}
	return &metadata, nil
}

func (p *platform) Logout(ctx context.Context, sid string) error {
	req, err := http.NewRequest(http.MethodGet, conf.AuthServerURL+":"+conf.AuthServerPort+"/logout", nil)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("sid", sid)
	res, err := p.client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode >= http.StatusMultipleChoices {
		return fmt.Errorf("status code is wrong - %d", res.StatusCode)
	}
	return nil
}

func (p *platform) GetProfile(token, idToken string) (map[string]interface{}, error) {
	req, err := http.NewRequest(http.MethodGet, conf.AuthServerURL+":"+conf.AuthServerPort+"/verify", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", token)
	req.Header.Add("IDToken", idToken)
	res, err := p.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode >= http.StatusMultipleChoices {
		fmt.Println(string(body))
		return nil, fmt.Errorf("status code is wrong - %d", res.StatusCode)
	}
	var result map[string]interface{}
	if err = json.Unmarshal(body, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (p *platform) CatchMessages(topic string, metadata Metadata, chMsg chan []byte) context.CancelFunc {
	ctx, cancel := context.WithCancel(context.Background())
	go p.queue.StartConsumer(ctx, topic, chMsg)
	go topicWatcher(ctx, chMsg, metadata)
	return cancel
}

func (p *platform) CatchMessagesWithGroupID(topic string, metadata Metadata, chMsg chan []byte) context.CancelFunc {
	ctx, cancel := context.WithCancel(context.Background())
	go p.queue.StartConsumerWithGroupID(ctx, getGroupID(metadata.UserID, topic), conf.KafkaServerVersion, []string{topic}, chMsg)
	go topicWatcher(ctx, chMsg, metadata)
	return cancel
}

func (p *platform) SendMessage(topic string, messages [][]byte) error {
	return p.queue.PushCommentToQueue(topic, messages)
}

func (p *platform) InitMsg(metadata Metadata) {
	msg := Message{Metadata: &metadata, Origin: metadata.UserID, Type: initType, Destination: metadata.UserID, Topic: metadata.UserID, Timestamp: time.Now().Unix()}
	b, _ := json.Marshal(&msg)
	if err := p.SendMessage(broadcast, [][]byte{b}); err != nil {
		log.Errorf("[func: push_comment_to_queue_with_conn] [error: %+v]", err)
		return
	}
}

func (p *platform) UserHealthMsg(metadata Metadata) {
	msg := Message{Metadata: &metadata, Origin: metadata.UserID, Type: healthCheckType, Destination: metadata.UserID, Topic: metadata.UserID, Timestamp: time.Now().Unix()}
	b, _ := json.Marshal(&msg)
	if err := p.SendMessage(broadcast, [][]byte{b}); err != nil {
		log.Errorf("[func: push_comment_to_queue_with_conn] [error: %+v]", err)
		return
	}
}

func (p *platform) HelpMsg(text string, metadata Metadata) {
	msg := Message{Metadata: &metadata, Text: text, Origin: metadata.UserID, Type: helpType, Destination: metadata.UserID, Topic: metadata.UserID, Timestamp: time.Now().Unix()}
	b, _ := json.Marshal(&msg)
	if err := p.SendMessage(broadcast, [][]byte{b}); err != nil {
		log.Errorf("[func: push_comment_to_queue_with_conn] [error: %+v]", err)
		return
	}
}

func (p *platform) UserInfoMsg(metadata, metadataInfo Metadata) {
	msg := Message{Metadata: &metadataInfo, Origin: metadata.UserID, Type: userInfoType, Destination: metadata.UserID, Topic: metadata.UserID, Timestamp: time.Now().Unix()}
	b, _ := json.Marshal(&msg)
	if err := p.SendMessage(broadcast, [][]byte{b}); err != nil {
		log.Errorf("[func: push_comment_to_queue_with_conn] [error: %+v]", err)
		return
	}
}

func (p *platform) UserOnMsg(metadata Metadata) {
	msg := Message{Metadata: &metadata, Origin: metadata.UserID, Type: userOnType, Destination: metadata.UserID, Topic: broadcast, Timestamp: time.Now().Unix()}
	b, _ := json.Marshal(&msg)
	if err := p.SendMessage(broadcast, [][]byte{b}); err != nil {
		log.Errorf("[func: user_on_msg] [error: %+v]", err)
	}
}

func (p *platform) UserOffMsg(metadata Metadata) {
	msg := Message{Metadata: &metadata, Origin: metadata.UserID, Type: userOffType, Destination: metadata.UserID, Topic: broadcast, Timestamp: time.Now().Unix()}
	b, _ := json.Marshal(&msg)
	if err := p.SendMessage(broadcast, [][]byte{b}); err != nil {
		log.Errorf("[func: user_off_msg] [error: %+v]", err)
	}
}

func topicWatcher(ctx context.Context, chMsg chan []byte, metadata Metadata) {
	defer recover()
FOR:
	for {
		select {
		case msg := <-chMsg:
			message := Message{}
			if err := json.Unmarshal(msg, &message); err != nil {
				continue FOR
			}
			if message.Type == initType || message.Type == userInfoType || message.Type == helpType || message.Type == healthCheckType {
				if metadata.UserID == message.Destination {
					b, _ := json.Marshal(&message)
					if err := Clients[metadata.UserID].Conn.WriteMessage(websocket.TextMessage, b); err != nil {
						log.Errorf("[func: topic_watcher] [error: %+v]", err)
					}
				}
				continue FOR
			}
			if message.Type == userOnType || message.Type == userOffType {
				if metadata.UserID != message.Destination {
					b, _ := json.Marshal(&message)
					if err := Clients[metadata.UserID].Conn.WriteMessage(websocket.TextMessage, b); err != nil {
						log.Errorf("[func: topic_watcher] [error: %+v]", err)
					}
				}
				continue FOR
			}
			if metadata.UserID == message.Origin {
				continue FOR
			}
			log.Debugf("[origin: %s] [destination: %s] [text: %s]", message.Origin, message.Destination, message.Text)
			b, _ := json.Marshal(&message)
			if err := Clients[metadata.UserID].Conn.WriteMessage(websocket.TextMessage, b); err != nil {
				log.Errorf("[func: topic_watcher] [error: %+v]", err)
			}
		case <-ctx.Done():
			log.Debug("[func: topic_watcher] [status: finished]")
			close(chMsg)
			return
		}
	}
}

func getGroupID(userID, topic string) string {
	hsha2 := sha256.Sum256([]byte(userID + topic))
	return hex.EncodeToString(hsha2[:])
}
