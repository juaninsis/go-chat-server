package chat

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
	"gitlab.com/juaninsis/go-chat-server/internal/cfg"
)

var (
	conf                = cfg.GetConfig()
	incomingConnections = make(chan Client)
	Clients             = make(ClientConn)
)

func init() {
	go func() {
		for client := range incomingConnections {
			Clients[client.Metadata.UserID] = client
		}
	}()
}

func Serve(upgrader WebSocketUpgrade, a Actioner) {
	http.HandleFunc("/chat", handleChat(upgrader, a))
}

func handleChat(upgrader WebSocketUpgrade, a Actioner) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		conn, _ := upgrader.Upgrade(writer, request, nil)
		defer conn.Close()
		var (
			token   = request.Header.Get("Authorization")
			idToken = request.Header.Get("IDToken")
		)

		profile, err := a.GetProfile(context.Background(), token, idToken)
		if err != nil {
			msg := Message{
				Error: &Error{Type: "verify_error", Text: err.Error()},
			}
			b, _ := json.Marshal(&msg)
			conn.WriteMessage(websocket.TextMessage, b)
			return
		}
		b, _ := json.Marshal(&profile)
		metadata := Metadata{}
		json.Unmarshal(b, &metadata)
		metadata.UserID = strings.Replace(metadata.UserID, "|", "", -1)
		incomingConnections <- Client{Username: metadata.Nickname, Conn: conn, Metadata: metadata}

		a.SetConnection(conn, metadata)
	}
}
