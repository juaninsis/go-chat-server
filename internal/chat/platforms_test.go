package chat

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
)

type RoundTripFunc func(req *http.Request) *http.Response

func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

func TestCatchMessages(t *testing.T) {
	qm := MockQueue{}
	qm.On("StartConsumer", mock.Anything, mock.Anything, mock.Anything)
	p := NewPlatform(nil, nil, &qm)
	chMsg := make(chan []byte)
	cancel := p.CatchMessages("", Metadata{}, chMsg)
	cancel()
}
func TestCatchMessagesWithGroupID(t *testing.T) {
	qm := MockQueue{}
	qm.On("StartConsumerWithGroupID", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything)
	p := NewPlatform(nil, nil, &qm)
	chMsg := make(chan []byte)
	cancel := p.CatchMessagesWithGroupID("", Metadata{}, chMsg)
	cancel()
}

func TestTopicWatcher(t *testing.T) {
	wsm := MockWebSocketConnecter{}
	ctx, cancel := context.WithCancel(context.Background())
	wsm.On("WriteMessage", mock.Anything, mock.Anything).Return(errors.New("some error"))
	chMsg := make(chan []byte, 5)
	chMsg <- []byte(`{type":"init","destination":"someone"}`)
	chMsg <- []byte(`{"type":"init","destination":"someone"}`)
	chMsg <- []byte(`{"type":"user_on","destination":"somebody"}`)
	chMsg <- []byte(`{"type":"chat","destination":"someone","origin":"someone"}`)
	chMsg <- []byte(`{"type":"chat","destination":"someone"}`)
	metadata := Metadata{UserID: "someone"}
	Clients["someone"] = Client{Username: "someone", Conn: &wsm, Metadata: metadata}
	go func(cancelF context.CancelFunc) {
		time.Sleep(2 * time.Second)
		cancelF()
	}(cancel)
	topicWatcher(ctx, chMsg, metadata)
}

func TestUserOffMsg(t *testing.T) {
	qm := MockQueue{}
	qm.On("PushCommentToQueue", mock.Anything, mock.Anything).Return(errors.New("some error"))
	p := NewPlatform(nil, nil, &qm)
	p.UserOffMsg(Metadata{})
}

func TestUserOnMsg(t *testing.T) {
	qm := MockQueue{}
	qm.On("PushCommentToQueue", mock.Anything, mock.Anything).Return(errors.New("some error"))
	p := NewPlatform(nil, nil, &qm)
	p.UserOnMsg(Metadata{})
}

func TestInitMsg(t *testing.T) {
	qm := MockQueue{}
	qm.On("PushCommentToQueue", mock.Anything, mock.Anything).Return(errors.New("some error"))
	p := NewPlatform(nil, nil, &qm)
	p.InitMsg(Metadata{})
}

func Test2xxGetProfile(t *testing.T) {
	jsonBody := `{"name":"someone"}`

	mClient := &http.Client{Transport: RoundTripFunc(func(req *http.Request) *http.Response {
		if req.URL.String() != conf.AuthServerURL+":"+conf.AuthServerPort+"/verify" {
			t.Error("expected request to "+conf.AuthServerURL+":"+conf.AuthServerPort+"/verify"+", got", req.URL.String())
		}
		if req.Method != http.MethodGet {
			t.Error("expected request method to be GET, got", req.Method)
		}
		if req.Header.Get("Content-Type") != "application/json" {
			t.Error("expected request Content-Type header to be application/json, got", req.Header.Get("Content-Type"))
		}

		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       ioutil.NopCloser(strings.NewReader(jsonBody)),
		}
	})}

	p := NewPlatform(nil, mClient, nil)

	profile, err := p.GetProfile("token", "543512380477")
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
	b, _ := json.Marshal(&profile)
	fmt.Println(jsonBody, string(b))
	assert.Equal(t, jsonBody, string(b))
}

func Test5xxErrorGetProfile(t *testing.T) {
	sid := "543512380477"
	mClient := &http.Client{Transport: RoundTripFunc(func(req *http.Request) *http.Response {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       ioutil.NopCloser(strings.NewReader(`Internal server error`)),
		}
	})}

	p := NewPlatform(nil, mClient, nil)

	_, err := p.GetProfile("", sid)
	if err == nil {
		t.Error("expected error")
	}
}

func Test2xxLogout(t *testing.T) {
	jsonBody := `{"name":"someone"}`

	mClient := &http.Client{Transport: RoundTripFunc(func(req *http.Request) *http.Response {
		if req.URL.String() != conf.AuthServerURL+":"+conf.AuthServerPort+"/logout" {
			t.Error("expected request to "+conf.AuthServerURL+":"+conf.AuthServerPort+"/logout"+", got", req.URL.String())
		}
		if req.Method != http.MethodGet {
			t.Error("expected request method to be GET, got", req.Method)
		}
		if req.Header.Get("Content-Type") != "application/json" {
			t.Error("expected request Content-Type header to be application/json, got", req.Header.Get("Content-Type"))
		}

		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       ioutil.NopCloser(strings.NewReader(jsonBody)),
		}
	})}

	p := NewPlatform(nil, mClient, nil)

	err := p.Logout(context.Background(), "543512380477")
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}
}

func Test5xxErrorLogout(t *testing.T) {
	sid := "543512380477"
	mClient := &http.Client{Transport: RoundTripFunc(func(req *http.Request) *http.Response {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       ioutil.NopCloser(strings.NewReader(`Internal server error`)),
		}
	})}

	p := NewPlatform(nil, mClient, nil)

	err := p.Logout(context.Background(), sid)
	if err == nil {
		t.Error("expected error")
	}
}
