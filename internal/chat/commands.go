package chat

import (
	"strings"

	"gitlab.com/juaninsis/go-chat-server/internal/errors"
)

const (
	cmdChat       = "/chat"
	cmdGroup      = "/group"
	cmdGeneral    = "/general"
	cmdTest       = "/test"
	cmdAddContact = "/add_contact"
	cmdHelp       = "/help"
	cmdHealth     = "/health"
)

var (
	cmdList = map[string]string{
		cmdChat:       "<user_id>   jump into a private chat with a member",
		cmdGroup:      "<group_name>   jump into a group with two or more members",
		cmdGeneral:    "jump to general chat with all members",
		cmdAddContact: "<user_id>   add a new member to your contact list",
		cmdHelp:       "show commands",
	}
)

func GetCmds() string {
	result := "The Chat commands are:\n"
	for k, v := range cmdList {
		result += "\t" + k + "  " + v + "\n"
	}
	return result
}

func getCommand(s string) (string, string, error) {
	s = strings.ToLower(s)
	if isChat(s) ||
		isGroup(s) ||
		isInfo(s) {
		cmds := strings.Split(strings.Replace(s, "|", "", -1), " ")
		if len(cmds) > 1 && cmds[1] != "" {
			return cmds[0], cmds[1], nil
		}
		return "", "", errors.NewBadCommand()
	}
	if isGeneral(s) {
		return cmdGeneral, "", nil
	}
	if isHealth(s) {
		return cmdHealth, "", nil
	}
	if isHelp(s) {
		return cmdHelp, GetCmds(), nil
	}
	if isTest(s) {
		return cmdTest, "", nil
	}

	return "", "", errors.NewCommandNotFound(s)
}

func isInfo(s string) bool {
	return validateCmd(s, cmdAddContact)
}

func isChat(s string) bool {
	return validateCmd(s, cmdChat)
}

func isGroup(s string) bool {
	return validateCmd(s, cmdGroup)
}

func isGeneral(s string) bool {
	return validateCmd(s, cmdGeneral)
}

func isHelp(s string) bool {
	return validateCmd(s, cmdHelp)
}

func isHealth(s string) bool {
	return validateCmd(s, cmdHealth)
}

func isTest(s string) bool {
	return validateCmd(s, cmdTest)
}

func validateCmd(s, cmd string) bool {
	return len(cmd) <= len(s) && cmd == s[:len(cmd)]
}
