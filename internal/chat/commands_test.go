package chat

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCommand(t *testing.T) {
	tests := []struct {
		name        string
		input       string
		outputCmd   string
		outputValue string
		err         error
	}{
		{
			name:        "successful chat get_command case",
			input:       "/chat new_chat",
			outputCmd:   cmdChat,
			outputValue: "new_chat",
		},
		{
			name:        "successful group get_command case",
			input:       "/group new_chat",
			outputCmd:   cmdGroup,
			outputValue: "new_chat",
		},
		{
			name:  "bad_command error group get_command case",
			input: "/group ",
			err:   errors.New("bad_command"),
		},
		{
			name:        "successful general get_command case",
			input:       "/general",
			outputCmd:   cmdGeneral,
			outputValue: "",
		},
		{
			name: "error get_command case",
			err:  errors.New("command [] not found"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			cmd, value, err := getCommand(test.input)
			if err != nil {
				assert.Equal(t, test.err.Error(), err.Error())
				return
			}
			assert.Equal(t, test.outputCmd, cmd)
			assert.Equal(t, test.outputValue, value)
		})
	}
}
