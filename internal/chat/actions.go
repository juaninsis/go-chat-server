package chat

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/juaninsis/go-chat-server/internal/errors"
)

const (
	broadcast       = "broadcast"
	privateType     = "private"
	generalType     = "general"
	groupType       = "group"
	initType        = "init"
	chatServer      = "chat-server"
	userOnType      = "user_on"
	userOffType     = "user_off"
	userInfoType    = "user_info"
	helpType        = "help"
	healthCheckType = "health_check"
	healthTime      = 2
)

var (
	broker = conf.KafkaServerURL + ":" + conf.KafkaServerPort
)

type (
	action struct {
		platform Platformer
	}
	Actioner interface {
		GetProfile(ctx context.Context, token, idToken string) (map[string]interface{}, error)
		SetConnection(conn WebSocketConnecter, metadata Metadata)
	}
	WebSocketConnecter interface {
		ReadMessage() (messageType int, p []byte, err error)
		WriteMessage(messageType int, data []byte) error
		Close() error
	}
	WebSocketUpgrade interface {
		Upgrade(w http.ResponseWriter, r *http.Request, responseHeader http.Header) (*websocket.Conn, error)
	}
)

func NewAction(platform Platformer) Actioner {
	return &action{platform: platform}
}

func (a *action) GetProfile(ctx context.Context, token, idToken string) (map[string]interface{}, error) {
	return a.platform.GetProfile(token, idToken)
}

func (a *action) SetConnection(conn WebSocketConnecter, metadata Metadata) {
	var (
		destination     = broadcast
		destinationType = generalType
		groups          = groupsM{
			broadcast: func() MsgInfo {
				chMsg := make(chan []byte)
				return MsgInfo{CtxCancel: a.platform.CatchMessages(broadcast, metadata, chMsg), ChMsg: chMsg}
			}(),
			metadata.UserID: func() MsgInfo {
				chMsg := make(chan []byte)
				return MsgInfo{CtxCancel: a.platform.CatchMessagesWithGroupID(metadata.UserID, metadata, chMsg), ChMsg: chMsg}
			}(),
		}
	)
	cant := 2
	chClosingSession := make(chan struct{}, cant)

	defer func() {
		_ = recover()
		for i := 0; i < cant; i++ {
			chClosingSession <- struct{}{}
		}
		log.Debug("[func: set_connection] [status: logout]")
		a.closingSession(groups, metadata)
	}()
	a.establishingSession(metadata)
	go a.sendHealthCheck(metadata, chClosingSession)
	chHealth := make(chan struct{})
	go a.userHealthCheckWorker(&groups, metadata, chHealth, chClosingSession)

FOR:
	for {
		_, message, _ := conn.ReadMessage()
		if len(message) <= 0 {
			continue
		}
		msg := Message{}
		if err := json.Unmarshal(message, &msg); err != nil {
			log.Errorf("[func: unmarshal] [error: %+v]", err)
			continue
		}
		cmd, value, err := getCommand(msg.Text)
		if err == nil || !errors.Is(err, &errors.CommandNotFound{}) {
			switch cmd {
			case cmdChat:
				log.Debug("[func: catch_message] [status: cmd_chat]")
				if value != metadata.UserID {
					destination, destinationType = value, privateType
					groups.addConsumer(a.platform, metadata.UserID, metadata)
				}
			case cmdGroup:
				log.Debug("[func: catch_message] [status: cmd_group]")
				if value != metadata.UserID {
					destination, destinationType = value, groupType+" | "+value
					groups.addConsumer(a.platform, destination, metadata)
				}
			case cmdGeneral:
				log.Debug("[func: catch_message] [status: cmd_general]")
				destination, destinationType = broadcast, generalType
				groups.addConsumer(a.platform, metadata.UserID, metadata)
			case cmdAddContact:
				log.Debug("[func: catch_message] [status: cmd_add_contact]")
				userInfo, err := a.platform.GetProfileCache(context.Background(), value)
				if err != nil {
					log.Errorf("[func: cmd_info] [error: %+v]", err)
					continue FOR
				}
				userInfo.SessionID = ""
				a.platform.UserInfoMsg(metadata, *userInfo)
			case cmdHelp:
				a.platform.HelpMsg(value, metadata)
			case cmdHealth:
				if msg.Type == healthCheckType {
					go func() {
						chHealth <- struct{}{}
					}()
				}
			case cmdTest:
				log.Panic("something wrong")
			}
			continue FOR
		}
		msg.Metadata, msg.Origin, msg.Destination, msg.Type = &metadata, metadata.UserID, destination, destinationType
		b, _ := json.Marshal(&msg)
		if err := a.platform.SendMessage(destination, [][]byte{b}); err != nil {
			log.Errorf("[func: push_comment_to_queue_with_conn] [error: %+v]", err)
		}
	}
}

func (a *action) establishingSession(metadata Metadata) {
	log.Infof("[user_id: %s] [sid: %s] [status: user_connected]\n", metadata.UserID, metadata.SessionID)
	a.platform.SetProfileCache(context.Background(), metadata)
	a.platform.InitMsg(metadata)
	a.platform.UserOnMsg(metadata)
}

func (a *action) closingSession(groups groupsM, metadata Metadata) {
	defer recover()
	log.Infof("the user %s disconnected\n", metadata.Nickname)
	a.platform.UserOffMsg(metadata)
	a.platform.Logout(context.Background(), metadata.SessionID)
	for topic := range groups {
		if v, ok := groups[topic]; ok {
			v.CtxCancel()
		}
		log.Debugf("[func: logout_consumers] [ topic: %s]\n", topic)
	}
	Clients.Delete(metadata.UserID)
	log.Debug(groups)
}

func (a *action) userHealthCheckWorker(groups *groupsM, metadata Metadata, chHealth, chClosingSession chan struct{}) {
FOR:
	for {
		timer := time.NewTimer(healthTime * 2 * time.Second)
		select {
		case <-chHealth:
			continue
		case <-timer.C:
			log.Debug("[func: user_health_check_worker] [status: logout]")
			a.closingSession(*groups, metadata)
			break FOR
		case <-chClosingSession:
			break FOR
		}
	}
}

func (a *action) sendHealthCheck(metadata Metadata, chClosingSession chan struct{}) {
FOR:
	for {
		timer := time.NewTimer(healthTime * time.Second)
		select {
		case <-timer.C:
			a.platform.UserHealthMsg(metadata)
		case <-chClosingSession:
			break FOR
		}
	}
}
