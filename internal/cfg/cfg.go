package cfg

import (
	"github.com/joeshaw/envdecode"
)

var (
	config Config
)

type (
	Config struct {
		OwnURL             string `env:"OWN_URL,default=http://127.0.0.1"`
		Port               string `env:"PORT,default=5554"`
		LogLevel           int64  `env:"LOG_LEVEL,default=5"`
		LogPath            string `env:"LOG_PATH,default=/tmp/logs/microservice.log"`
		AuthServerURL      string `env:"AUTH_SERVER_URL,default=http://localhost"`
		AuthServerPort     string `env:"AUTH_SERVER_PORT,default=4444"`
		KafkaServerURL     string `env:"KAFKA_SERVER_URL,default=localhost"`
		KafkaServerPort    string `env:"KAFKA_SERVER_PORT,default=9092"`
		KafkaServerVersion string `env:"KAFKA_SERVER_VERSION,default=7.3.2"`
		RedisHost          string `env:"REDIS_HOST,default=redis-chat-server"`
		RedisPort          string `env:"REDIS_PORT,default=6379"`
	}
)

func initCfg() {
	if err := envdecode.Decode(&config); err != nil {
		panic(err)
	}
}

func GetConfig() Config {
	initCfg()
	return config
}
