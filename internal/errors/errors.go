package errors

import (
	"errors"
)

func Is(err, target error) bool {
	return errors.Is(err, target)
}
func New(msg string) error {
	return errors.New(msg)
}

type (
	CommandNotFound struct {
		err error
	}
	BadCommand struct {
		err error
	}
)

func (e *BadCommand) Error() string {
	return e.err.Error()
}

func (e *BadCommand) Is(err error) bool {
	_, ok := err.(*BadCommand)
	return ok
}

func (e *CommandNotFound) Error() string {
	return e.err.Error()
}

func (e *CommandNotFound) Is(err error) bool {
	_, ok := err.(*CommandNotFound)
	return ok
}

func NewBadCommand() *BadCommand {
	return &BadCommand{New("bad_command")}
}

func NewCommandNotFound(msg string) *CommandNotFound {
	return &CommandNotFound{New("command [" + msg + "] not found")}
}
