package errors

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCommandNotFound(t *testing.T) {
	err := NewCommandNotFound("/test")
	assert.Equal(t, "command [/test] not found", err.Error())
	if !errors.Is(err, &CommandNotFound{}) {
		t.Fail()
	}

	if otherErr := errors.New("/test"); errors.Is(otherErr, &CommandNotFound{}) {
		t.Fail()
	}
}
