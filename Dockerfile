FROM golang:1.19 as builder

ENV CGO_ENABLED=0

WORKDIR /app

RUN git clone https://gitlab.com/juaninsis/go-chat-server.git
WORKDIR /app/go-chat-server
RUN go get -d -v ./...
RUN go build -o ./build/ cmd/server.go

#############

FROM alpine:latest

COPY --from=builder /app/go-chat-server/build .

CMD ["./server"]
